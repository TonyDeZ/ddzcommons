#ifndef DZSTONE_GLOBAL_H
#define DZSTONE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(DZSTONE_LIBRARY)
#  define DZSTONESHARED_EXPORT Q_DECL_EXPORT
#else
#  define DZSTONESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // DZSTONE_GLOBAL_H
