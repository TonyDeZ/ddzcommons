#-------------------------------------------------
#
# Project created by QtCreator 2016-02-02T00:13:58
#
#-------------------------------------------------

QT       -= gui

TARGET = dzStone
TEMPLATE = lib

DEFINES += DZSTONE_LIBRARY

SOURCES += dzStone.cpp

HEADERS += dzStone.h\
        dzstone_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
