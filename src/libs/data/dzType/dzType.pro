#-------------------------------------------------
#
# Project created by QtCreator 2016-02-02T00:13:40
#
#-------------------------------------------------

QT       -= gui

TARGET = dzType
TEMPLATE = lib

DEFINES += DZTYPE_LIBRARY

SOURCES += dzType.cpp

HEADERS += dzType.h\
        dztype_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
