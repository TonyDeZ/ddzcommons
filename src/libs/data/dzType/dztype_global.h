#ifndef DZTYPE_GLOBAL_H
#define DZTYPE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(DZTYPE_LIBRARY)
#  define DZTYPESHARED_EXPORT Q_DECL_EXPORT
#else
#  define DZTYPESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // DZTYPE_GLOBAL_H
