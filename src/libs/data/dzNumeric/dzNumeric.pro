#-------------------------------------------------
#
# Project created by QtCreator 2016-02-02T00:14:18
#
#-------------------------------------------------

QT       -= gui

TARGET = dzNumeric
TEMPLATE = lib

DEFINES += DZNUMERIC_LIBRARY

SOURCES += dzNumeric.cpp

HEADERS += dzNumeric.h\
        dznumeric_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
