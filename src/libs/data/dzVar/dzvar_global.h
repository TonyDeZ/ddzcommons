#ifndef DZVAR_GLOBAL_H
#define DZVAR_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(DZVAR_LIBRARY)
#  define DZVARSHARED_EXPORT Q_DECL_EXPORT
#else
#  define DZVARSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // DZVAR_GLOBAL_H
