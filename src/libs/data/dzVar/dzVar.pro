#-------------------------------------------------
#
# Project created by QtCreator 2016-02-02T00:14:33
#
#-------------------------------------------------

QT       -= gui

TARGET = dzVar
TEMPLATE = lib

DEFINES += DZVAR_LIBRARY

SOURCES += dzVar.cpp

HEADERS += dzVar.h\
        dzvar_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
