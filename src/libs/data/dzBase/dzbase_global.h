#ifndef DZBASE_GLOBAL_H
#define DZBASE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(DZBASE_LIBRARY)
#  define DZBASESHARED_EXPORT Q_DECL_EXPORT
#else
#  define DZBASESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // DZBASE_GLOBAL_H
