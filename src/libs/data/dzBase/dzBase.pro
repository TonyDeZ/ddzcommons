#-------------------------------------------------
#
# Project created by QtCreator 2016-02-02T00:13:11
#
#-------------------------------------------------

QT       -= gui

TARGET = dzBase
TEMPLATE = lib

DEFINES += DZBASE_LIBRARY

SOURCES += dzBase.cpp

HEADERS += dzBase.h\
        dzbase_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
